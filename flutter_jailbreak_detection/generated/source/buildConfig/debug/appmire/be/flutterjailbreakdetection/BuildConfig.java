/**
 * Automatically generated file. DO NOT MODIFY
 */
package appmire.be.flutterjailbreakdetection;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "appmire.be.flutterjailbreakdetection";
  public static final String BUILD_TYPE = "debug";
}
