/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.emarteveryday;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.emarteveryday";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 23041901;
  public static final String VERSION_NAME = "2.0.7";
}
