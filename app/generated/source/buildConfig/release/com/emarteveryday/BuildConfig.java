/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.emarteveryday;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.emarteveryday";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 23051801;
  public static final String VERSION_NAME = "2.0.7";
}
